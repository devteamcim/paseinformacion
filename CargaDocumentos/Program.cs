﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CargaDocumentos
{
    class Program
    {
        static void Main(string[] args)
        {
            Codigo.cManejo cMan = new Codigo.cManejo();
            int iArchivos = 0;

            if (args.Length > 0)
                // iArchivos = cMan.Carga(new List<string>(args));
                iArchivos = 0;
            else
                iArchivos = cMan.Carga();

            if (cMan.Valido)
                Console.WriteLine(String.Format("Se cargaron {0} archivos exitosamente", iArchivos));
            else
                Console.WriteLine(String.Format("Ocurrió error: {0}", cMan.Error));
        }
    }
}
