﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace CargaDocumentos.Codigo
{
    class cManejo
    {
        private int piCount;                                                // Número de archivos procesados
        private string psError;                                             // Descripción del posible error
        private bool pbValido;                                              // Estatus final de la última función

        public string Error
        {
            get
            {
                return psError;
            }
        }

        public bool Valido
        {
            get
            {
                return pbValido;
            }
        }

        public int Archivo
        {
            get
            {
                return piCount;
            }
        }

        private class rFile
        {
            public int ID
            {
                get;
                set;
            }
            public string Name
            {
                get;
                set;
            }
        }

        public int Carga()
        {
            Codigo.cRutas oRutas = new cRutas();
            return CargaArchivos(oRutas.Rutas());
        }

        //public int Carga(List<string> aOrigen)
        //{
        //    return CargaArchivos(aOrigen);
        //}

        private int CargaArchivos(List<cRutas.Ruta> aOrigen)
        {
            string[] aMasc = Mascara().ToString().Split(';');

            piCount = 0;                                            // Contador de archivos a 0
            psError = "";                                           // Limpiar mensaje de error
            pbValido = true;                                        // Por omisión, todo sale bien
            List<rFile> aArchivos = new List<rFile>();
            cBitacora oBita = new cBitacora();
            List<string> aRutas = new List<string>();

            foreach (cRutas.Ruta rOrigen in aOrigen)
            {
                Console.WriteLine("Preparando para subir los archivos desde {0}", rOrigen.Origen);

                if (Directory.Exists(rOrigen.Origen))
                {
                    aArchivos = new List<rFile>();

                    foreach (string sMascara in aMasc)
                    {
                        string sMask = sMascara.Trim();
                        foreach (string sFile in Directory.GetFiles(rOrigen.Origen, sMask))
                        {
                            rFile rArc = new rFile();
                            rArc.Name = sFile;
                            rArc.ID = rOrigen.ID;
                            aArchivos.Add(rArc);
                        }
                    }
                }
                else
                {
                    oBita.AgregaBitacora(5, "CargaDocumentos", rOrigen.Origen, "", "", "No existe el directorio de origen");
                    aRutas.Add(rOrigen.Origen);
                }

                ProcesaArchivos(aArchivos);

                if (aRutas.Count > 0)
                {
                    cMail oMail = new cMail();
                    oMail.EnviaCorreo("Directorios con error", "", aRutas);
                }
            }

            return piCount;
        }

        private void ProcesaArchivos(List<rFile> aArchivos)
        {
            cBitacora oBita = new cBitacora();

            using (SqlConnection oDB = new SqlConnection(Properties.Settings.Default.Conexion))
            {
                if (oDB.State == ConnectionState.Closed)
                    oDB.Open();

                // Cada archivo dentro de la lista lo subimos a la Base de Datos
                foreach (rFile rArc in aArchivos)
                {
                    FileInfo xFile = new FileInfo(rArc.Name);                                   // Para obtener la información del archivo
                    try
                    {
                        FileStream oArch = File.Open(rArc.Name, FileMode.Open);                 // Abrimos el archivo
                        int iLong = Convert.ToInt32(oArch.Length);                              // Obtenemos la longitud en bytes
                        byte[] aByte = new byte[iLong];                                         // Creamos un arreglo de bytes para contener el archivo

                        oArch.Read(aByte, 0, iLong);                                            // Cargamos el archivo a memoria

                        using (SqlCommand oQry = new SqlCommand("[ibd].[ups_InsertaPaseDocumentos]", oDB))
                        {
                            oQry.CommandType = CommandType.StoredProcedure;

                            oQry.Parameters.AddWithValue("@Contenido", aByte);
                            oQry.Parameters.AddWithValue("@Original", rArc.Name);
                            oQry.Parameters.AddWithValue("@Nombre", xFile.Name);
                            oQry.Parameters.AddWithValue("@Tipo", xFile.Extension.Replace(".", "").ToUpper());
                            oQry.Parameters.AddWithValue("@Central", rArc.ID);

                            piCount += oQry.ExecuteNonQuery();
                        }
                    }
                    catch (Exception ex)
                    {
                        pbValido = false;
                        psError = ex.Message.ToString();
                        int iRes = oBita.AgregaBitacora(2, "CargaDocumentos", xFile.DirectoryName, "", xFile.Name, psError);
                        break;
                    }
                }
            }
        }

        private string Mascara()
        {
            // Obtenemos las máscaras a utilizar 
            string sMascara = "";

            using (SqlConnection oDB = new SqlConnection(Properties.Settings.Default.Conexion))
            {
                if (oDB.State == ConnectionState.Closed)
                    oDB.Open();
                using(SqlCommand oQry = new SqlCommand("[ibd].[usp_MascarasPaseDocumentos]", oDB))
                {
                    oQry.CommandType = CommandType.StoredProcedure;
                    using(SqlDataReader rDatos = oQry.ExecuteReader())
                    {
                        if (rDatos.Read())
                        {
                            sMascara = rDatos.IsDBNull(rDatos.GetOrdinal("Mascara")) ? "" : rDatos.GetString(rDatos.GetOrdinal("Mascara"));
                        }
                    }
                }
            }

            return sMascara;
        }
    }
}
