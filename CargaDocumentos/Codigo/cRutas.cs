﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace CargaDocumentos.Codigo
{
    class cRutas
    {
        public class Ruta
        {
            public int Numero
            {
                get;
                set;
            }
            public int ID
            {
                get;
                set;
            }
            public string Origen
            {
                get;
                set;
            }
            public int Prioridad
            {
                get;
                set;
            }
        }

        public List<Ruta> Rutas()
        {
            List<Ruta> aRutas = new List<Ruta>();

            using (SqlConnection oDB = new SqlConnection(Properties.Settings.Default.Conexion))
            {
                if (oDB.State == ConnectionState.Closed)
                    oDB.Open();
                using (SqlCommand oQry = new SqlCommand("[ibd].[usp_spsRutasOrigenPaseDocumentos]", oDB))
                {
                    oQry.CommandType = CommandType.StoredProcedure;
                    using (SqlDataReader rDatos = oQry.ExecuteReader())
                    {
                        while (rDatos.Read())
                        {
                            Ruta rRuta = new Ruta();
                            rRuta.Numero = rDatos.IsDBNull(rDatos.GetOrdinal("rtdNumero")) ? 0 : rDatos.GetInt32(rDatos.GetOrdinal("rtdNumero"));
                            rRuta.ID = rDatos.IsDBNull(rDatos.GetOrdinal("idCentral")) ? 0 : rDatos.GetInt32(rDatos.GetOrdinal("idCentral"));
                            rRuta.Origen = rDatos.IsDBNull(rDatos.GetOrdinal("rtdRuta")) ? "" : rDatos.GetString(rDatos.GetOrdinal("rtdRuta"));
                            rRuta.Prioridad = rDatos.IsDBNull(rDatos.GetOrdinal("rtdPrioridad")) ? 0 : rDatos.GetInt32(rDatos.GetOrdinal("rtdPrioridad"));

                            if (!String.IsNullOrEmpty(rRuta.Origen))
                            {
                                aRutas.Add(rRuta);
                            }
                        }
                    }
                }
            }
            return aRutas;
        }
    }
}
